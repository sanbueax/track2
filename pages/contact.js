import React, { useState, useEffect } from 'react';
import { Row, Card, Form, Button } from 'react-bootstrap'
// import Head from 'next/head'
import NavBar from '../components/NavBar'
import View from '../components/View'
import emailjs from 'emailjs-com';
import MessengerCustomerChat from 'react-messenger-customer-chat';

export default function contact() {
  const [subject, setSubject] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  function sendEmail(e){
    e.preventDefault();

    emailjs.sendForm('service_feuh65r', 'template_va49cwc', e.target, 'user_euk1rzXZ4Jv8kuDu95WH2')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });

    e.target.reset();
  }
  return (
    <React.Fragment>
        <NavBar/>
        <View title={ 'Contact Us' }>
            <Row className="justify-content-center">
              <Card className="mt-5">
              <Card.Header className="text-center">Send Us Email</Card.Header>
              <Card.Body>
                <Form onSubmit={(e) => sendEmail(e)}>
                  <Form.Group controlId="subject">
                      <Form.Label>Subject</Form.Label>
                      <Form.Control type="text" placeholder="Subject" value={subject} onChange={e => setSubject(e.target.value)} name="subject" required/>
                  </Form.Group>            

                  <Form.Group controlId="email">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} name="email" required/>
                  </Form.Group>

                  <Form.Group controlId="password1">
                      <Form.Label>Message</Form.Label>
                      <Form.Control type="text" placeholder="Your message" value={message} onChange={e => setMessage(e.target.value)} name="message" required/>
                  </Form.Group>

                  <Button className="w-100 text-center d-flex justify-content-center mt-3" variant="primary" type="submit" id="submitBtn">Submit</Button>

                  {/* {isActive ?
                      <Button className="w-100 text-center d-flex justify-content-center mt-3" variant="primary" type="submit" id="submitBtn">Submit</Button>
                      :
                      <Button className="w-100 text-center d-flex justify-content-center mt-3" variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                  } */}
                </Form>
              </Card.Body>
            </Card>
          </Row>
        </View>
        <MessengerCustomerChat pageId="102192615074160" appId="828657124376265" />
    </React.Fragment>
  )
}
