import React from 'react';
import { Row, Card } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import View from '../components/View'

export default function contact() {
    return (
    <React.Fragment>
        <NavBar/>
        <View title={ 'Track2' }>
            <Row className="justify-content-center">
                <div id="card-email" class="card-series col-sm-6 col-md-4 col-lg-4 mt-5">
                    <div class="card zoom shadow card-fixed mb-4 text-center">
                        <div class="card-header bg-info text-light">Email <span class="badge badge-pill badge-danger">0</span></div>
                        <div class="card-body">
                            <h6 class="text-secondary">No Email</h6>
                        </div>
                    </div>
                </div>
                <div id="card-email" class="card-series col-sm-6 col-md-4 col-lg-4 mt-5">
                    <div class="card zoom shadow card-fixed mb-4 text-center">
                        <div class="card-header bg-info text-light">Facebook <span class="badge badge-pill badge-danger">0</span></div>
                        <div class="card-body">
                            <h6 class="text-secondary">No Messages</h6>
                        </div>
                    </div>
                </div>
                <div id="card-email" class="card-series col-sm-6 col-md-4 col-lg-4 mt-5">
                    <div class="card zoom shadow card-fixed mb-4 text-center">
                        <div class="card-header bg-info text-light">Viber <span class="badge badge-pill badge-danger">0</span></div>
                        <div class="card-body">
                            <h6 class="text-secondary">No Messages</h6>
                        </div>
                    </div>
                </div>
            </Row>
        </View>
    </React.Fragment>
  )
}
