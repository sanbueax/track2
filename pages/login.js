import React, { useState, useEffect, useContext } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
// import { GoogleLogin } from 'react-google-login'
// import NavBar from '../components/LoginNav'
import Router from 'next/router'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import AppHelper from '../apphelper'
import View from '../components/View'
// import Link from 'next/link'
// import Logedin from '../styles/Login.module.css'

export default function login() {

    useEffect(() => {
        Swal.fire({
            title: 'Are you an Admin?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'green',
            cancelButtonColor: 'rgb(212,175,55)',
            confirmButtonText: 'Yes, I am!',
            allowOutsideClick: false
        })
    }, [Swal]) 
    
    return (
        <React.Fragment>
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Login/>
            </Row>
        </View>
        </React.Fragment>
    ) 
}

const Login = () => {
    const { user, setUser } = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        
        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                }
            }
        })
    }
    
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
            console.log(data)
            setUser({ email: data.email })

            Router.push('/')
        })
    }

    return (
        <Card className="mt-5">
            <Card.Header className="text-center">Login</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control type="email" value={ email } onChange={ (e) => setEmail(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={ password } onChange={ (e) => setPassword(e.target.value) } required/>
                    </Form.Group>
                    <Button className="mb-2" variant="primary" type="submit" block>Login</Button>
                </Form>
            </Card.Body>
        </Card>
    )
}