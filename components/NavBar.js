import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NavBar() {
    const { user } = useContext(UserContext)

	return(
      <Navbar className="bgcolor" bg="light" expand="lg" fixed="top">
        <Navbar.Brand href="/">Track 2</Navbar.Brand>
        <Navbar.Toggle aria-control="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link href="/">Home</Nav.Link>
            {user.email 
            ? <React.Fragment> 
                <Nav.Link href="/admin">Messages</Nav.Link>
                <Nav.Link href="/logout">Logout</Nav.Link>
                </React.Fragment>
            : <React.Fragment>
                <Nav.Link href="/contact">Contact Us</Nav.Link>
                <Nav.Link href="/login">Login</Nav.Link>
              </React.Fragment>
            }
          </Nav>
        </Navbar.Collapse>        
      </Navbar>
	)
}